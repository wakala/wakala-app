import 'package:app_wakala/features/login/bloc/login_bloc_bloc.dart';
import 'package:app_wakala/features/login/ui/login.dart';
import 'package:app_wakala/features/wakala/bloc/wakala_bloc_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';

void main() async {
  await dotenv.load(fileName: "lib/.env");
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider<WakalaBlocBloc>(
          create: (BuildContext context) => WakalaBlocBloc(),
        ),
        BlocProvider<LoginBlocBloc>(
          create: (BuildContext context) => LoginBlocBloc(),
        ),
      ],
      child: MaterialApp(
        title: 'Flutter Demo',
        theme: ThemeData(
          colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
          useMaterial3: true,
        ),
        home: const LoginView(),
      ),
    );
  }
}
