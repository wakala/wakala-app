import 'package:app_wakala/features/login/bloc/login_bloc_bloc.dart';
import 'package:app_wakala/features/wakala/ui/list.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class LoginView extends StatefulWidget {
  const LoginView({super.key});

  @override
  State<LoginView> createState() => _LoginViewState();
}

class _LoginViewState extends State<LoginView> {
  final formKey = GlobalKey<FormState>();
  final username = TextEditingController();
  final password = TextEditingController();
  @override
  void initState() {
    super.initState();
    context.read<LoginBlocBloc>().add(AutoLoginEvent());
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    password.dispose();
    username.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<LoginBlocBloc, LoginBlocState>(
      bloc: BlocProvider.of<LoginBlocBloc>(context),
      listenWhen: (previus, current) {
        return true;
      },
      buildWhen: (previous, current) {
        return true;
      },
      listener: (context, state) {
        if (state is LoginSuccessState || state is AutoLoginSuccesState) {
          Navigator.of(context).push(
            MaterialPageRoute(
              builder: (BuildContext context) => const WakalaList(),
            ),
          );
        } else if (state is LoginFailedState) {
          ScaffoldMessenger.of(context).showSnackBar(SnackBar(
            content: Text(state.message),
            backgroundColor: Colors.red[700],
          ));
        }

        username.clear();
        password.clear();
      },
      builder: (context, state) {
        switch (state.runtimeType) {
          case const (LoginLoadingState):
            return const Scaffold(
              body: Center(
                child: CircularProgressIndicator(),
              ),
            );
          case const (LoginBlocInitial):
            return Scaffold(
              bottomSheet: Container(
                color: Theme.of(context).canvasColor,
                child: const Padding(
                  padding: EdgeInsets.all(16.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      Text(
                        "by",
                        style: TextStyle(fontSize: 20),
                      ),
                      Text("Tomas Bravo\nIvan Montti"),
                    ],
                  ),
                ),
              ),
              body: Form(
                key: formKey,
                child: Padding(
                  padding: const EdgeInsets.all(40.0),
                  child: ListView(
                    children: [
                      const Text(
                        "Wakalas 1.0",
                        textAlign: TextAlign.center,
                      ),
                      const SizedBox.square(dimension: 20),
                      const Text(
                        "Usuario",
                      ),
                      TextField(
                        controller: username,
                        decoration: const InputDecoration(
                          contentPadding: EdgeInsets.all(20),
                          filled: true,
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.all(
                              Radius.circular(20),
                            ),
                            borderSide: BorderSide.none,
                          ),
                        ),
                      ),
                      const SizedBox.square(dimension: 20),
                      const Text(
                        "Password",
                      ),
                      TextField(
                        controller: password,
                        decoration: const InputDecoration(
                          contentPadding: EdgeInsets.all(20),
                          filled: true,
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.all(
                              Radius.circular(20),
                            ),
                            borderSide: BorderSide.none,
                          ),
                        ),
                        obscureText: true,
                      ),
                      const SizedBox.square(
                        dimension: 20,
                      ),
                      ElevatedButton(
                          onPressed: () {
                            if (formKey.currentState!.validate()) {
                              BlocProvider.of<LoginBlocBloc>(context).add(
                                  LoginSubmitEvent(
                                      username: username.text,
                                      pasword: password.text));
                            }
                          },
                          child: const Text('Iniciar Sesion'))
                    ],
                  ),
                ),
              ),
            );
          default:
            return const Scaffold(
              body: Center(child: CircularProgressIndicator()),
            );
        }
      },
    );
  }
}
