import 'dart:async';
import 'dart:convert';
import 'dart:developer';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:http/http.dart' as http;

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:shared_preferences/shared_preferences.dart';

part 'login_bloc_event.dart';
part 'login_bloc_state.dart';

class LoginBlocBloc extends Bloc<LoginBlocEvent, LoginBlocState> {
  late SharedPreferences localData;

  LoginBlocBloc() : super(LoginBlocInitial()) {
    on<LoginSubmitEvent>(loginAction);
    on<AutoLoginEvent>(tryAutoLogin);
    on<LogoutEvent>(logout);
  }

  FutureOr<void> loginAction(
      LoginSubmitEvent event, Emitter<LoginBlocState> emit) async {
    localData = await SharedPreferences.getInstance();
    var client = http.Client();
    emit(LoginLoadingState());
    try {
      final response = await client.post(
          Uri.parse("${dotenv.env['API_URL']}user/login"),
          body: {"username": event.username, "password": event.pasword});
      final data = jsonDecode(response.body);
      if (response.statusCode == 200) {
        await localData.setString("token", data["data"]["token"]);
        emit(LoginSuccessState(data['message']));
      } else {
        emit(LoginFailedState(data['message']));
      }
    } catch (e) {
      log(e.toString());
      emit(LoginFailedState(e.toString()));
    }
    emit(LoginBlocInitial());
  }

  FutureOr<void> tryAutoLogin(
      AutoLoginEvent event, Emitter<LoginBlocState> emit) async {
    emit(LoginLoadingState());
    localData = await SharedPreferences.getInstance();
    if (localData.containsKey("token")) {
      emit(AutoLoginSuccesState());
    } else {
      emit(AutoLoginFailedState());
    }
    emit(LoginBlocInitial());
  }

  FutureOr<void> logout(LogoutEvent event, Emitter<LoginBlocState> emit) async {
    try {
      final SharedPreferences data = await SharedPreferences.getInstance();
      await data.remove("token");
      emit(LogoutState());
    } catch (e) {
      log(e.toString());
    }
    emit(LoginBlocInitial());
  }
}
