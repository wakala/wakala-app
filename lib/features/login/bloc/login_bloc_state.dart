part of 'login_bloc_bloc.dart';

@immutable
sealed class LoginBlocState {}

final class LoginBlocInitial extends LoginBlocState {}

final class LoginLoadingState extends LoginBlocState {}

final class LoginSuccessState extends LoginBlocState {
  final String message;
  LoginSuccessState(this.message);
}

final class LoginFailedState extends LoginBlocState {
  final String message;
  LoginFailedState(this.message);
}

final class AutoLoginSuccesState extends LoginBlocState {}

final class AutoLoginFailedState extends LoginBlocState {}

final class LogoutState extends LoginBlocState {}
