part of 'login_bloc_bloc.dart';

@immutable
sealed class LoginBlocEvent {}

class LoginSubmitEvent extends LoginBlocEvent {
  final String username;
  final String pasword;

  LoginSubmitEvent({required this.username, required this.pasword}) {
    log(pasword);
    log(username);
  }
}

class AutoLoginEvent extends LoginBlocEvent {}

class LogoutEvent extends LoginBlocEvent {}
