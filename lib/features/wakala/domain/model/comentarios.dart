import 'package:app_wakala/features/wakala/domain/model/user.dart';

class Comentario {
  late String contenido;
  late DateTime updateAt;
  late DateTime createdAt;
  late User user;

  Comentario(this.contenido, this.createdAt, this.updateAt, this.user);

  Comentario.fromJson(Map<String, dynamic> json) {
    contenido = json['contenido'];
    updateAt = DateTime.parse(json['updateAt']);
    createdAt = DateTime.parse(json['createdAt']);
    user = User.fromJson(json['user']);
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['contenido'] = contenido;
    data['updateAt'] = updateAt;
    data['createdAt'] = createdAt;
    data['user'] = user.toJson();
    return data;
  }
}
