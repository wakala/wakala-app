import 'package:app_wakala/features/wakala/domain/model/comentarios.dart';
import 'package:app_wakala/features/wakala/domain/model/wakala.dart';

class WakalaData {
  late Wakala wakala;
  List<Comentario> comments = [];

  WakalaData(this.wakala, this.comments);

  WakalaData.fromJson(Map<String, dynamic> json) {
    wakala = Wakala.fromJson(json);
    for (var comment in json['comentarios']) {
      comments.add(Comentario.fromJson(comment as Map<String, dynamic>));
    }
  }
}
