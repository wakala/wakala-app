import 'package:app_wakala/features/wakala/domain/model/user.dart';

class Wakala {
  late String id;
  late String sector;
  late String descripcion;
  late String foto1;
  late String foto2;
  late String si;
  late String no;
  late User user;
  late String createdAt;
  late String updateAt;

  Wakala(
    this.id,
    this.sector,
    this.descripcion,
    this.foto1,
    this.foto2,
    this.si,
    this.no,
    this.user,
    this.createdAt,
    this.updateAt,
  );

  Wakala.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    sector = json['sector'];
    descripcion = json['descripcion'];
    foto1 = json['foto1'];
    foto2 = json['foto2'];
    si = json['si'];
    no = json['no'];
    user = User.fromJson(json['user']);
    createdAt = json['createdAt'];
    updateAt = json['updateAt'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['sector'] = sector;
    data['descripcion'] = descripcion;
    data['foto1'] = foto1;
    data['foto2'] = foto2;
    data['si'] = si;
    data['no'] = no;
    data['user'] = user;
    data['createdAt'] = createdAt;
    data['updateAt'] = updateAt;
    return data;
  }
}
