import 'package:app_wakala/features/wakala/domain/model/wakala.dart';

class WakalaRepository {
  List<Wakala> wakalas = [];

  WakalaRepository.fromJson(List<dynamic> json) {
    for (var element in json.reversed) {
      wakalas.add(Wakala.fromJson(element as Map<String, dynamic>));
    }
  }
}
