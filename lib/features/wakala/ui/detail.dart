import 'package:app_wakala/features/wakala/bloc/wakala_bloc_bloc.dart';
import 'package:app_wakala/features/wakala/ui/comment.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:intl/intl.dart';

import '../domain/model/comentarios.dart';
import '../domain/model/wakala.dart';

class WakalaDetailView extends StatefulWidget {
  final Wakala wakala;
  const WakalaDetailView({super.key, required this.wakala});

  @override
  State<WakalaDetailView> createState() => _WakalaDetailViewState();
}

class _WakalaDetailViewState extends State<WakalaDetailView> {
  late Wakala wakala;
  @override
  void initState() {
    super.initState();
    wakala = widget.wakala;
    context.read<WakalaBlocBloc>().add(GetWakalaEvent(wakala.id));
    context.read<WakalaBlocBloc>().add(GetCommentsEvent(wakala.id));
  }

  @override
  Widget build(BuildContext context) {
    WakalaBlocBloc wakalaBloc = BlocProvider.of<WakalaBlocBloc>(context);
    List<Comentario> data = [];
    return BlocConsumer<WakalaBlocBloc, WakalaBlocState>(
      bloc: wakalaBloc,
      buildWhen: (previous, current) => current is! WakalaActionState,
      listenWhen: (previous, current) => true,
      listener: (context, state) {
        if (state is GetWakalaSuccessState) {
          wakala = state.data;
        } else if (state is GoListAction) {
          Navigator.of(context).pop();
          wakalaBloc.add(GetListEvent());
        } else if (state is GoCommentAction) {
          Navigator.of(context).push(MaterialPageRoute(
            builder: (context) => CommentView(wakala: wakala),
          ));
        }
      },
      builder: (context, state) {
        switch (state.runtimeType) {
          case const (GetCommentsSuccess):
            data = (state as GetCommentsSuccess).data;
            return detailScaffold(wakalaBloc, data, false, wakala);
          case const (GetCommentsLodiangState):
            return detailScaffold(wakalaBloc, data, true, wakala);
          case const (VotoSuccess):
            wakala = (state as VotoSuccess).data;
            return detailScaffold(wakalaBloc, data, false, wakala);
          case const (VotoLoading):
            return detailScaffold(wakalaBloc, data, true, wakala);
          default:
            return detailScaffold(wakalaBloc, data, false, wakala);
        }
      },
    );
  }

  Scaffold detailScaffold(WakalaBlocBloc wakalaBloc, List<Comentario> data,
      bool loading, Wakala wakala) {
    return Scaffold(
      appBar: AppBar(
        title: Text(wakala.sector),
        centerTitle: true,
        automaticallyImplyLeading: false,
      ),
      body: !loading
          ? Padding(
              padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 20),
              child: ListView(
                children: [
                  Text(
                    wakala.descripcion,
                    textAlign: TextAlign.justify,
                  ),
                  const SizedBox.square(
                    dimension: 20,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      Image.network(
                        "${dotenv.env['API_URL']}wakala/image/${wakala.foto1}",
                        width: 150,
                        height: 200,
                      ),
                      if (wakala.foto2.isNotEmpty) ...[
                        Image.network(
                          "${dotenv.env['API_URL']}wakala/image/${wakala.foto2}",
                          width: 150,
                          height: 200,
                        ),
                      ],
                    ],
                  ),
                  const SizedBox.square(
                    dimension: 10,
                  ),
                  Text(
                      "Subido por @${wakala.user.username} el ${DateFormat('dd/mm/yyyy').format(DateTime.parse(wakala.createdAt))}"),
                  const SizedBox.square(
                    dimension: 10,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      ElevatedButton(
                        onPressed: () {
                          wakalaBloc.add(VotoEvent(wakala.id, true));
                        },
                        child: Text("Sigue ahi (${wakala.si})"),
                      ),
                      ElevatedButton(
                        onPressed: () {
                          wakalaBloc.add(VotoEvent(wakala.id, false));
                        },
                        child: Text("No sigue ahi (${wakala.no})"),
                      ),
                    ],
                  ),
                  const SizedBox.square(
                    dimension: 20,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      const Text("Comentarios"),
                      ElevatedButton(
                          onPressed: () {
                            wakalaBloc.add(GoCommentEvent());
                          },
                          child: const Text('Comentar'))
                    ],
                  ),
                  const SizedBox.square(
                    dimension: 20,
                  ),
                  if (data.isNotEmpty) ...[
                    SizedBox(
                      height: 200,
                      child: ListView.builder(
                        itemBuilder: (context, index) {
                          Comentario comment = data[index];

                          return ListTile(
                            title: Text(comment.contenido),
                            subtitle: Text(
                              "por @${comment.user.username}",
                              textAlign: TextAlign.end,
                            ),
                          );
                        },
                        itemCount: data.length,
                      ),
                    )
                  ] else ...[
                    const Text("No hay Comentarios")
                  ],
                  const SizedBox.square(
                    dimension: 10,
                  ),
                  ElevatedButton(
                      onPressed: () {
                        wakalaBloc.add(WakalaGoListAction());
                      },
                      child: const Text("Volver A Listado"))
                ],
              ),
            )
          : const Center(child: CircularProgressIndicator()),
    );
  }
}
