import 'package:app_wakala/features/login/bloc/login_bloc_bloc.dart';
import 'package:app_wakala/features/wakala/bloc/wakala_bloc_bloc.dart';
import 'package:app_wakala/features/wakala/domain/repository/wakala_repo.dart';
import 'package:app_wakala/features/wakala/ui/create.dart';
import 'package:app_wakala/features/wakala/ui/detail.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';

class WakalaList extends StatefulWidget {
  const WakalaList({super.key});

  @override
  State<WakalaList> createState() => _WakalaListState();
}

class _WakalaListState extends State<WakalaList> {
  @override
  void initState() {
    super.initState();
    BlocProvider.of<WakalaBlocBloc>(context).add(GetListEvent());
  }

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<LoginBlocBloc, LoginBlocState>(
      listener: (context, state) {
        if (state is LogoutState) {
          ScaffoldMessenger.of(context).showSnackBar(SnackBar(
            content: const Text("Cierre de sesion exitoso"),
            backgroundColor: Colors.green[200],
          ));
          Navigator.of(context).pop();
        }
      },
      builder: (context, state) {
        return BlocConsumer(
            bloc: BlocProvider.of<WakalaBlocBloc>(context),
            buildWhen: (previous, current) => current is! WakalaActionState,
            listenWhen: (previous, current) => current is WakalaActionState,
            listener: (context, state) {
              if (state is GoCreateAction) {
                Navigator.of(context).push(
                  MaterialPageRoute(
                    builder: (context) => const NewWakalaView(),
                  ),
                );
              } else if (state is GoDetailAction) {
                Navigator.of(context).push(
                  MaterialPageRoute(
                    builder: (context) => WakalaDetailView(
                      wakala: state.data,
                    ),
                  ),
                );
              }
            },
            builder: (context, state) {
              switch (state.runtimeType) {
                case const (LoadingState):
                  return const Scaffold(
                    body: Center(
                      child: CircularProgressIndicator(),
                    ),
                  );
                case const (GetWakalaListSuccessState):
                  WakalaRepository repo =
                      (state as GetWakalaListSuccessState).repo;
                  return Scaffold(
                      appBar: AppBar(
                        title: const Text("Listado de Wakalas"),
                        automaticallyImplyLeading: false,
                        actions: [
                          TextButton(
                              onPressed: () =>
                                  BlocProvider.of<LoginBlocBloc>(context)
                                      .add(LogoutEvent()),
                              child: const Icon(Icons.logout))
                        ],
                      ),
                      floatingActionButton: FloatingActionButton(
                        onPressed: () {
                          BlocProvider.of<WakalaBlocBloc>(context)
                              .add(WakalaGoCreateAction());
                        },
                        child: const Icon(Icons.add),
                      ),
                      body: Padding(
                        padding: const EdgeInsets.symmetric(
                            vertical: 10, horizontal: 20),
                        child: ListView.builder(
                          itemBuilder: (context, index) => ListTile(
                            title: Text(repo.wakalas[index].sector),
                            subtitle: Text(
                              "por ${repo.wakalas[index].user.username} ${DateFormat("dd/mm/yyyy").format(DateTime.parse(repo.wakalas[index].createdAt))}",
                            ),
                            onTap: () {
                              BlocProvider.of<WakalaBlocBloc>(context).add(
                                WakalaGoDetailAction(
                                    wakala: repo.wakalas[index]),
                              );
                            },
                            trailing:
                                const Icon(Icons.keyboard_arrow_right_rounded),
                          ),
                          itemCount: repo.wakalas.length,
                        ),
                      ));
                default:
                  return const Scaffold(
                    body: Center(
                      child: Text("No hay Wakalas que Mostrar"),
                    ),
                  );
              }
            });
      },
    );
  }
}
