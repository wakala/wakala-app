import 'package:app_wakala/features/wakala/bloc/wakala_bloc_bloc.dart';
import 'package:app_wakala/features/wakala/domain/model/wakala.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class CommentView extends StatelessWidget {
  final Wakala wakala;
  const CommentView({super.key, required this.wakala});

  @override
  Widget build(BuildContext context) {
    WakalaBlocBloc wakalaBloc = BlocProvider.of<WakalaBlocBloc>(context);
    final comentario = TextEditingController();
    final formKey = GlobalKey<FormState>();
    return Scaffold(
      appBar: AppBar(
        title: Text(wakala.sector),
        automaticallyImplyLeading: false,
        centerTitle: true,
      ),
      body: BlocConsumer<WakalaBlocBloc, WakalaBlocState>(
        bloc: wakalaBloc,
        buildWhen: (previous, current) => current is! WakalaActionState,
        listenWhen: (previous, current) => true,
        builder: (context, state) {
          switch (state.runtimeType) {
            case const (CommentSubmitLoadingState):
              return const Center(
                child: CircularProgressIndicator(),
              );
            default:
              return Padding(
                padding:
                    const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                child: Form(
                  key: formKey,
                  child: ListView(
                    children: [
                      TextFormField(
                        decoration: const InputDecoration(
                          contentPadding: EdgeInsets.all(20),
                          filled: true,
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.all(Radius.circular(20)),
                            borderSide: BorderSide.none,
                          ),
                        ),
                        maxLines: 10,
                        controller: comentario,
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return "Debe ingresar un valor de comentario";
                          }
                          return null;
                        },
                      ),
                      const SizedBox.square(
                        dimension: 20,
                      ),
                      ElevatedButton(
                        onPressed: () {
                          if (formKey.currentState!.validate()) {
                            wakalaBloc.add(WakalaSubmitCommetEvent(
                                comentario.text, wakala.id));
                          }
                        },
                        child: const Text("Comentar Wakala"),
                      ),
                      const SizedBox.square(
                        dimension: 20,
                      ),
                      ElevatedButton(
                        style: ElevatedButton.styleFrom(
                            backgroundColor: Colors.red.shade100,
                            foregroundColor: Colors.red.shade900),
                        onPressed: () {
                          wakalaBloc.add(WakalaBackDetailEvent());
                        },
                        child: const Text("Me Arrepiento"),
                      )
                    ],
                  ),
                ),
              );
          }
        },
        listener: (context, state) {
          if (state is GoBackDetailAction) {
            Navigator.of(context).pop();
          } else if (state is CommentSubmitSuccessState) {
            Navigator.of(context).pop();
            ScaffoldMessenger.of(context).showSnackBar(
              SnackBar(
                content: Text(state.message),
                backgroundColor: Colors.green[100],
              ),
            );
            wakalaBloc.add(GetCommentsEvent(wakala.id));
          }
        },
      ),
    );
  }
}
