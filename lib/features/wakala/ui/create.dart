import 'dart:developer';
import 'dart:io';

import 'package:app_wakala/features/wakala/bloc/wakala_bloc_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:image_picker/image_picker.dart';

class NewWakalaView extends StatefulWidget {
  const NewWakalaView({super.key});

  @override
  State<NewWakalaView> createState() => _NewWakalaViewState();
}

class _NewWakalaViewState extends State<NewWakalaView> {
  File? foto1;
  bool isFoto1 = false;
  File? foto2;
  bool isFoto2 = false;
  final _formKey = GlobalKey<FormState>();
  final sector = TextEditingController();
  final descripcion = TextEditingController();

  @override
  void dispose() {
    super.dispose();
    sector.dispose();
    descripcion.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final WakalaBlocBloc wakalaBloc = BlocProvider.of<WakalaBlocBloc>(context);
    return BlocConsumer<WakalaBlocBloc, WakalaBlocState>(
      bloc: wakalaBloc,
      builder: (context, state) {
        switch (state.runtimeType) {
          case const (WakalaSubmitWaitState):
            return const Scaffold(
              body: Center(
                child: CircularProgressIndicator(),
              ),
            );
          default:
            return Scaffold(
              appBar: AppBar(
                title: const Text("Avisar nuevo Wakala"),
                centerTitle: true,
                automaticallyImplyLeading: false,
              ),
              body: Form(
                  key: _formKey,
                  child: Padding(
                    padding: const EdgeInsets.all(20.0),
                    child: ListView(
                      children: [
                        const Row(children: [
                          Text("Sector"),
                          Text(
                            ' *',
                            style: TextStyle(color: Colors.red),
                          ),
                        ]),
                        const SizedBox(
                          height: 10,
                        ),
                        TextFormField(
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return 'Valor Requerido';
                            }
                            return null;
                          },
                          controller: sector,
                          decoration: const InputDecoration(
                            contentPadding: EdgeInsets.all(20),
                            filled: true,
                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.all(
                                Radius.circular(20),
                              ),
                              borderSide: BorderSide.none,
                            ),
                          ),
                        ),
                        const SizedBox(
                          height: 20,
                        ),
                        const Row(
                          children: [
                            Text('Descripcion'),
                            Text(
                              ' *',
                              style: TextStyle(color: Colors.red),
                            ),
                          ],
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        TextFormField(
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return 'Valor Requerido';
                            }
                            return null;
                          },
                          controller: descripcion,
                          decoration: const InputDecoration(
                            contentPadding: EdgeInsets.all(20),
                            filled: true,
                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.all(
                                Radius.circular(20),
                              ),
                              borderSide: BorderSide.none,
                            ),
                          ),
                          maxLines: 6,
                          minLines: 4,
                        ),
                        const SizedBox(
                          height: 20,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            Column(
                              children: [
                                const Row(
                                  children: [
                                    Text("Foto 1"),
                                    Text(
                                      " *",
                                      style: TextStyle(color: Colors.red),
                                    ),
                                  ],
                                ),
                                const SizedBox(
                                  height: 10,
                                ),
                                if (!isFoto1) ...[
                                  ElevatedButton(
                                    style: ElevatedButton.styleFrom(
                                      fixedSize: const Size(150, 200),
                                      shape: const RoundedRectangleBorder(
                                        borderRadius: BorderRadius.all(
                                          Radius.circular(10),
                                        ),
                                      ),
                                    ),
                                    onPressed: () {
                                      selectImage(true);
                                    },
                                    child: const Icon(Icons.add),
                                  )
                                ] else ...[
                                  TextButton(
                                    onPressed: () {
                                      deleteImage(true);
                                    },
                                    style: TextButton.styleFrom(
                                      fixedSize: const Size(150, 200),
                                      shape: const RoundedRectangleBorder(
                                        borderRadius: BorderRadius.all(
                                          Radius.circular(10),
                                        ),
                                      ),
                                    ),
                                    child: Image(
                                      image: FileImage(foto1!),
                                      height: 200,
                                      width: 150,
                                    ),
                                  ),
                                ],
                              ],
                            ),
                            Column(
                              children: [
                                const Text("Foto 2"),
                                const SizedBox(
                                  height: 10,
                                ),
                                if (!isFoto2) ...[
                                  ElevatedButton(
                                    style: ElevatedButton.styleFrom(
                                      fixedSize: const Size(150, 200),
                                      shape: const RoundedRectangleBorder(
                                        borderRadius: BorderRadius.all(
                                          Radius.circular(10),
                                        ),
                                      ),
                                    ),
                                    onPressed: () {
                                      selectImage(false);
                                    },
                                    child: const Icon(Icons.add),
                                  )
                                ] else ...[
                                  Tooltip(
                                    message: "Hacer Click para eliminar imagen",
                                    triggerMode: TooltipTriggerMode.longPress,
                                    child: TextButton(
                                      onPressed: () {
                                        deleteImage(false);
                                      },
                                      style: TextButton.styleFrom(
                                        fixedSize: const Size(150, 200),
                                        shape: const RoundedRectangleBorder(
                                          borderRadius: BorderRadius.all(
                                            Radius.circular(10),
                                          ),
                                        ),
                                      ),
                                      child: Image(
                                        image: FileImage(foto2!),
                                        height: 200,
                                        width: 150,
                                      ),
                                    ),
                                  ),
                                ],
                              ],
                            ),
                          ],
                        ),
                        const SizedBox(
                          height: 20,
                        ),
                        ElevatedButton(
                          style: ElevatedButton.styleFrom(
                              backgroundColor: Colors.green[100],
                              foregroundColor: Colors.green[600]),
                          onPressed: () async {
                            if (_formKey.currentState!.validate()) {
                              if (isFoto1) {
                                wakalaBloc.add(
                                  WakalaCreateSubmitEvent(
                                    sector.text,
                                    descripcion.text,
                                    foto1!,
                                    foto2,
                                  ),
                                );
                                sector.clear();
                                descripcion.clear();
                              } else {
                                await showDialog(
                                  context: context,
                                  builder: (context) => AlertDialog(
                                    content: const Text(
                                      "Debes ingresar foto 1",
                                      textAlign: TextAlign.center,
                                    ),
                                    icon: const Icon(
                                      Icons.error,
                                      size: 36,
                                    ),
                                    iconColor: Colors.redAccent,
                                    actions: [
                                      TextButton(
                                          onPressed: () =>
                                              Navigator.pop(context, "Aceptar"),
                                          child: const Text("Aceptar"))
                                    ],
                                  ),
                                );
                              }
                            }
                          },
                          child: const Text("Denunciar Wakala"),
                        ),
                        const SizedBox(
                          height: 20,
                        ),
                        ElevatedButton(
                          style: ElevatedButton.styleFrom(
                              backgroundColor: Colors.red[100],
                              foregroundColor: Colors.red[600]),
                          onPressed: () {
                            wakalaBloc.add(WakalaGoListAction());
                          },
                          child: const Text("Me Arrepiento"),
                        ),
                      ],
                    ),
                  )),
            );
        }
      },
      listener: (context, state) async {
        if (state is WakalaCreateFailedState) {
          await showDialog(
            context: context,
            builder: (context) => AlertDialog(
              icon: const Icon(Icons.error),
              iconColor: Colors.red,
              backgroundColor: Colors.red[100],
              content: Text(
                state.messasge,
                textAlign: TextAlign.center,
              ),
              actions: [
                TextButton(
                  onPressed: () => Navigator.of(context).pop(),
                  child: const Text("Aceptar"),
                ),
              ],
              actionsAlignment: MainAxisAlignment.center,
            ),
          );
        } else if (state is WakalaSubmitSuccesState) {
          Navigator.of(context).pop();
          ScaffoldMessenger.of(context).showSnackBar(
            SnackBar(
              content: Text(state.message),
              backgroundColor: Colors.green[100],
            ),
          );
          wakalaBloc.add(GetListEvent());
        } else if (state is GoListAction) {
          Navigator.of(context).pop();
          wakalaBloc.add(GetListEvent());
        }
      },
      buildWhen: (previous, current) => current is! WakalaActionState,
      listenWhen: (previous, current) => true,
    );
  }

  deleteImage(bool foto) {
    if (foto) {
      setState(() {
        foto1 = null;
        isFoto1 = false;
      });
    } else {
      setState(() {
        foto2 = null;
        isFoto2 = false;
      });
    }
  }

  selectImage(bool foto) async {
    try {
      final pickedImage =
          await ImagePicker().pickImage(source: ImageSource.gallery);
      if (pickedImage != null) {
        setState(() {
          if (foto) {
            foto1 = File(pickedImage.path);
            isFoto1 = true;
          } else {
            foto2 = File(pickedImage.path);
            isFoto2 = true;
          }
        });
      } else {
        log('User didnt pick any image.');
      }
    } catch (e) {
      log(e.toString());
    }
  }
}
