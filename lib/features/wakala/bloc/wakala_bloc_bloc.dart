import 'dart:async';
import 'dart:convert';
import 'dart:developer';
import 'dart:io';

import 'package:app_wakala/features/wakala/domain/model/comentarios.dart';
import 'package:app_wakala/features/wakala/domain/repository/wakala_repo.dart';
import 'package:bloc/bloc.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import "package:meta/meta.dart";
import 'package:flutter/material.dart';

import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

import '../domain/model/wakala.dart';

part 'wakala_bloc_event.dart';
part 'wakala_bloc_state.dart';

class WakalaBlocBloc extends Bloc<WakalaBlocEvent, WakalaBlocState> {
  WakalaBlocBloc() : super(WakalaBlocInitial()) {
    on<GetListEvent>(getWakalaList);
    on<WakalaCreateSubmitEvent>(createWakala);
    on<WakalaGoCreateAction>(goCreateEvent);
    on<WakalaGoDetailAction>(goDetailAction);
    on<WakalaGoListAction>(goListAction);
    on<GoCommentEvent>(goCommentAction);
    on<WakalaBackDetailEvent>(goBackDetailAction);
    on<WakalaSubmitCommetEvent>(createComment);
    on<GetCommentsEvent>(getComments);
    on<VotoEvent>(votar);
    on<GetWakalaEvent>(getWakala);
  }

  FutureOr<void> getWakalaList(
      WakalaBlocEvent event, Emitter<WakalaBlocState> emit) async {
    final client = http.Client();
    emit(LoadingState());
    try {
      final SharedPreferences localData = await SharedPreferences.getInstance();
      String? token = localData.getString("token");
      final response = await client.get(
        Uri.parse("${dotenv.env['API_URL']!}wakala"),
        headers: {
          "Authorization": "Bearer $token",
        },
      );
      final data = json.decode(response.body)['wakalas'];
      final repo = WakalaRepository.fromJson(data);

      if (repo.wakalas.isNotEmpty) {
        emit(GetWakalaListSuccessState(repo));
      } else {
        emit(GetWakalaListEmptyState());
      }
    } catch (e) {
      log(e.toString());
      emit(GetWakalaListEmptyState());
    }
  }

  FutureOr<void> createWakala(
      WakalaCreateSubmitEvent event, Emitter<WakalaBlocState> emit) async {
    log(event.descripcion);
    log(event.sector);
    log(event.foto1.path);
    if (event.foto2 != null) log(event.foto2!.path);
    emit(WakalaSubmitWaitState());

    try {
      final request = http.MultipartRequest(
        "POST",
        Uri.parse("${dotenv.env['API_URL']!}wakala"),
      );
      request.fields['sector'] = event.sector;
      request.fields['descripcion'] = event.descripcion;
      if (event.foto2 != null) {
        request.files.addAll([
          http.MultipartFile.fromBytes(
            "fotos",
            event.foto1.readAsBytesSync(),
            filename: event.foto1.path,
          ),
          http.MultipartFile.fromBytes(
            "fotos",
            event.foto2!.readAsBytesSync(),
            filename: event.foto2!.path,
          )
        ]);
      } else {
        request.files.add(
          http.MultipartFile.fromBytes(
            "fotos",
            event.foto1.readAsBytesSync(),
            filename: event.foto1.path,
          ),
        );
      }
      final SharedPreferences localData = await SharedPreferences.getInstance();
      String? token = localData.getString("token");
      request.headers["Authorization"] = "Bearer $token";
      final streamRes = await request.send();
      final res = await http.Response.fromStream(streamRes);
      final data = jsonDecode(res.body);
      if (res.statusCode == 201) {
        emit(WakalaSubmitSuccesState(data['message']));
      } else {
        emit(WakalaCreateFailedState(data['message']));
      }
    } catch (e) {
      log(e.toString());
      emit(WakalaCreateFailedState(e.toString()));
    }
  }

  FutureOr<void> goCreateEvent(
      WakalaGoCreateAction event, Emitter<WakalaBlocState> emit) {
    emit(GoCreateAction());
  }

  FutureOr<void> goDetailAction(
      WakalaGoDetailAction event, Emitter<WakalaBlocState> emit) async {
    emit(GoDetailAction(data: event.wakala));
  }

  FutureOr<void> goListAction(
      WakalaGoListAction event, Emitter<WakalaBlocState> emit) {
    emit(GoListAction());
  }

  FutureOr<void> goCommentAction(
      GoCommentEvent event, Emitter<WakalaBlocState> emit) {
    emit(GoCommentAction());
  }

  FutureOr<void> goBackDetailAction(
      WakalaBackDetailEvent event, Emitter<WakalaBlocState> emit) {
    emit(GoBackDetailAction());
  }

  FutureOr<void> createComment(
      WakalaSubmitCommetEvent event, Emitter<WakalaBlocState> emit) async {
    final client = http.Client();
    emit(CommentSubmitLoadingState());
    try {
      final SharedPreferences localData = await SharedPreferences.getInstance();
      String? token = localData.getString("token");
      final res = await client.post(
          Uri.parse(
              "${dotenv.env['API_URL']!}wakala/${event.wakalaId}/comentarios"),
          headers: {
            "Authorization": "Bearer $token",
          },
          body: {
            "contenido": event.contenido,
          });
      emit(
        CommentSubmitSuccessState(message: json.decode(res.body)['message']),
      );
    } catch (e) {
      log(e.toString());
      emit(CommentSubmitFailedState(message: e.toString()));
    }
  }

  FutureOr<void> getComments(
      GetCommentsEvent event, Emitter<WakalaBlocState> emit) async {
    final client = http.Client();
    emit(GetCommentsLodiangState());
    try {
      final SharedPreferences localData = await SharedPreferences.getInstance();
      String? token = localData.getString("token");
      final res = await client.get(
        Uri.parse("${dotenv.env['API_URL']!}wakala/${event.id}/comentarios"),
        headers: {
          "Authorization": "Bearer $token",
        },
      );
      List<Comentario> data = (jsonDecode(res.body)["data"] as List<dynamic>)
          .map(
            (e) => Comentario.fromJson(e as Map<String, dynamic>),
          )
          .toList();
      emit(GetCommentsSuccess(data: data));
    } catch (e) {
      log(e.toString());
      emit(GetCommentsFailed(e.toString()));
    }
  }

  FutureOr<void> votar(VotoEvent event, Emitter<WakalaBlocState> emit) async {
    final client = http.Client();
    emit(VotoLoading());
    try {
      final SharedPreferences localData = await SharedPreferences.getInstance();
      String? token = localData.getString("token");
      final res = await client.put(
          Uri.parse("${dotenv.env['API_URL']!}wakala/${event.id}"),
          headers: {
            HttpHeaders.contentTypeHeader: "application/json",
            "Content-Type": "application/x-www-form-urlencoded",
            "Authorization": "Bearer $token",
          },
          body: {
            "voto": event.add.toString()
          });
      final data = jsonDecode(res.body);
      if (res.statusCode == 200) {
        emit(
          VotoSuccess(Wakala.fromJson(data["wakala"]), data['message']),
        );
      } else {
        emit(VotoFailed(data['message']));
      }
    } catch (e) {
      log(e.toString());
      emit(VotoFailed(e.toString()));
    }
  }

  FutureOr<void> getWakala(
      GetWakalaEvent event, Emitter<WakalaBlocState> emit) async {
    final client = http.Client();
    emit(GetWakalaLoadingState());
    try {
      final SharedPreferences localData = await SharedPreferences.getInstance();
      String? token = localData.getString("token");
      final res = await client.get(
        Uri.parse("${dotenv.env['API_URL']!}wakala/${event.id}"),
        headers: {
          "Authorization": "Bearer $token",
        },
      );
      final data = jsonDecode(res.body);
      final wakalaData =
          Wakala.fromJson(data['wakala'] as Map<String, dynamic>);
      emit(GetWakalaSuccessState(wakalaData));
    } catch (e) {
      log(e.toString());
      emit(GetWakalaFailedState(e.toString()));
    }
  }
}
