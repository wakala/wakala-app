part of 'wakala_bloc_bloc.dart';

@immutable
sealed class WakalaBlocState {}

final class WakalaBlocInitial extends WakalaBlocState {}

final class WakalaActionState extends WakalaBlocState {}

final class GoListAction extends WakalaActionState {}

final class GoCreateAction extends WakalaActionState {}

final class GoCommentAction extends WakalaActionState {}

final class GoBackDetailAction extends WakalaActionState {}

final class GoDetailAction extends WakalaActionState {
  final Wakala data;
  GoDetailAction({required this.data});
}

final class GoBackListAction extends WakalaActionState {}

final class GetWakalaListSuccessState extends WakalaBlocState {
  final WakalaRepository repo;
  GetWakalaListSuccessState(this.repo);
}

final class GetWakalaListEmptyState extends WakalaBlocState {}

final class WakalaSubmitWaitState extends WakalaBlocState {}

final class WakalaSubmitSuccesState extends WakalaBlocState {
  final String message;

  WakalaSubmitSuccesState(this.message);
}

final class WakalaCreateFailedState extends WakalaBlocState {
  final String messasge;

  WakalaCreateFailedState(this.messasge);
}

final class LoadingState extends WakalaBlocState {}

final class CommentSubmitLoadingState extends WakalaBlocState {}

final class CommentSubmitSuccessState extends WakalaBlocState {
  final String message;

  CommentSubmitSuccessState({required this.message});
}

final class CommentSubmitFailedState extends WakalaBlocState {
  final String message;

  CommentSubmitFailedState({required this.message});
}

final class GetCommentsSuccess extends WakalaBlocState {
  final List<Comentario> data;

  GetCommentsSuccess({required this.data});
}

final class GetCommentsLodiangState extends WakalaBlocState {}

final class GetCommentsFailed extends WakalaBlocState {
  final String message;

  GetCommentsFailed(this.message);
}

final class VotoSuccess extends WakalaBlocState {
  final Wakala data;
  final String message;

  VotoSuccess(this.data, this.message);
}

final class VotoLoading extends WakalaBlocState {}

final class VotoFailed extends WakalaBlocState {
  final String message;
  VotoFailed(this.message);
}

final class GetWakalaSuccessState extends WakalaBlocState {
  final Wakala data;

  GetWakalaSuccessState(this.data);
}

final class GetWakalaFailedState extends WakalaBlocState {
  final String message;

  GetWakalaFailedState(this.message);
}

final class GetWakalaLoadingState extends WakalaBlocState {}
