part of 'wakala_bloc_bloc.dart';

@immutable
sealed class WakalaBlocEvent {}

class WakalaGoDetailAction extends WakalaBlocEvent {
  final Wakala wakala;
  WakalaGoDetailAction({required this.wakala});
}

class WakalaGoCreateAction extends WakalaBlocEvent {}

class WakalaGoListAction extends WakalaBlocEvent {}

class GetListEvent extends WakalaBlocEvent {}

class GoBackToListEvent extends WakalaBlocEvent {}

class WakalaBackDetailEvent extends WakalaBlocEvent {}

class GoCommentEvent extends WakalaBlocEvent {
  // final Wakala wakala;
  // GoCommentEvent(this.wakala);
}

class WakalaCreateSubmitEvent extends WakalaBlocEvent {
  final String sector;
  final String descripcion;
  final File foto1;
  final File? foto2;

  WakalaCreateSubmitEvent(
      this.sector, this.descripcion, this.foto1, this.foto2);
}

class WakalaSubmitCommetEvent extends WakalaBlocEvent {
  final String contenido;
  final String wakalaId;
  WakalaSubmitCommetEvent(this.contenido, this.wakalaId);
}

class GetCommentsEvent extends WakalaBlocEvent {
  final String id;
  GetCommentsEvent(this.id);
}

class VotoEvent extends WakalaBlocEvent {
  final String id;
  final bool add;
  VotoEvent(this.id, this.add);
}

class GetWakalaEvent extends WakalaBlocEvent {
  final String id;

  GetWakalaEvent(this.id);
}
